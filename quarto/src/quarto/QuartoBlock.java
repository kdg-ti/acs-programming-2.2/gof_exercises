package quarto;

/*
 * Do not change anything in this class!
 */
public  class QuartoBlock {
	public enum Length {TALL, SHORT}

	public enum Color {DARK, LIGHT}

	public enum Shape {SQUARE, CIRCULAR}

	public enum Volume {HOLLOW, SOLID}

	private  Length length;
	private  Color color;
	private  Shape shape;
	private  Volume volume;

	public QuartoBlock(Length length, Color color, Shape shape, Volume volume) {
		this.length = length;
		this.color = color;
		this.shape = shape;
		this.volume = volume;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) return true;
		if (object == null || getClass() != object.getClass()) return false;

		QuartoBlock that = (QuartoBlock) object;

		return equals(that.color, that.length, that.shape, that.volume);

	}

	public Length getLength() {
		return length;
	}

	public void setLength(Length length) {
		this.length = length;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Shape getShape() {
		return shape;
	}

	public void setShape(Shape shape) {
		this.shape = shape;
	}

	public Volume getVolume() {
		return volume;
	}

	public void setVolume(Volume volume) {
		this.volume = volume;
	}

	public boolean equals(
		Color color,
		Length length,
		Shape shape,
		Volume volume
	) {
		return this.length == length && this.color == color && this.shape
			== shape && this.volume == volume;
	}





	@Override
	public int hashCode() {
		int result = length != null ? length.hashCode() : 0;
		result = 31 * result + (color != null ? color.hashCode() : 0);
		result = 31 * result + (shape != null ? shape.hashCode() : 0);
		result = 31 * result + (volume != null ? volume.hashCode() : 0);
		return result;
	}

	public String toString() {
		return "a " + length + ", " + color + ", " + shape + ", " + volume + " Quarto block";
	}

}