package composite;

/**
 * @author Jan de Rijke.
 */
public interface Container extends Component{
	public void add(Component c) ;
	public void remove(Component c) ;
}
