import queue.HelpDeskItem;
import queue.HelpDeskQueue;
import queue.Queue2List;


public class DemoQueue {
    public static void main(String[] args) {
        HelpDeskQueue myQueue = new Queue2List();

        myQueue.enqueue(new HelpDeskItem("Can't log in"));
        myQueue.enqueue(new HelpDeskItem(5, "Server crash"));
        myQueue.enqueue(new HelpDeskItem("Mouse not working"));
        myQueue.enqueue(new HelpDeskItem(5, "Laptop on fire!"));

        myQueue.overviewNatural();
        myQueue.overviewByPriority();
    }
}

/*
OUTPUT:
Queue in natural order:
	2016-10-18T13:55:24.146:  1 Can't log in
	2016-10-18T13:55:24.146:  5 Server crash
	2016-10-18T13:55:24.146:  1 Mouse not working
	2016-10-18T13:55:24.146:  5 Laptop on fire!
Queue by priority:
	2016-10-18T13:55:24.146:  5 Server crash
	2016-10-18T13:55:24.146:  5 Laptop on fire!
	2016-10-18T13:55:24.146:  1 Can't log in
	2016-10-18T13:55:24.146:  1 Mouse not working
 */
