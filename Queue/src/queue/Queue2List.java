package queue;

import java.util.ArrayList;
import java.util.List;

public class Queue2List implements HelpDeskQueue {
	// use this list for storing the elements in the queue
	private List<HelpDeskItem> list = new ArrayList<>();

	@Override
	public void enqueue(HelpDeskItem helpDeskItem) {
		//TODO : add item at the end
	}

	@Override
	public HelpDeskItem dequeue() {
		//TODO: remove item at the front and return
		return null;
	}

	@Override
	public void overviewByPriority() {
		// TODO: print queue items by priority first, then by LocalDateTime
	}

	@Override
	public void overviewNatural() {
		// TODO: print queue items FIFO
	}
}
