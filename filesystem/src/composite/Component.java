package composite;

/* Do not change anything here!
*/

public interface Component {

  long getSize();

  String getPath();

  void setParent(Directory parent);

}