package quarto;

/**
 * @author Jan de Rijke.
 */
public class QuartoTest {
  public static void main(String[] args) {
    QuartoBlock b1 = QuartoFactory.createBlock(
        QuartoBlock.Length.SHORT,
        QuartoBlock.Color.DARK,
        QuartoBlock.Shape.CIRCULAR,
        QuartoBlock.Volume.HOLLOW);

    QuartoBlock b2 = QuartoFactory.createBlock(
        QuartoBlock.Length.SHORT,
        QuartoBlock.Color.LIGHT,
        QuartoBlock.Shape.CIRCULAR,
        QuartoBlock.Volume.SOLID);

    QuartoBlock b3 = QuartoFactory.createBlock(
        QuartoBlock.Length.SHORT,
        QuartoBlock.Color.DARK,
        QuartoBlock.Shape.CIRCULAR,
        QuartoBlock.Volume.HOLLOW);

    QuartoBlock b4 = QuartoFactory.createBlock(
        QuartoBlock.Length.TALL,
        QuartoBlock.Color.DARK,
        QuartoBlock.Shape.SQUARE,
        QuartoBlock.Volume.HOLLOW);

    QuartoBlock b5 = QuartoFactory.createBlock(
        QuartoBlock.Length.TALL,
        QuartoBlock.Color.DARK,
        QuartoBlock.Shape.CIRCULAR,
        QuartoBlock.Volume.HOLLOW);
    System.out.printf("Factory has created %d blocks (should be 5)%n", QuartoFactory.countBlocks());
    System.out.println("Blocks 1 and 3 should be the same object: " + (b1 == b3));

  }
}
