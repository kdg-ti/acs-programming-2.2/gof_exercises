package composite;

import java.util.ArrayList;
import java.util.List;

public final class Directory implements Container {
  private final String name;
  private final List<Component> children;
  private Directory parent;

  public Directory(String name){
    this.name = name;
    this.parent = null;
    children = new ArrayList<Component>();
  }

  @Override
  public long getSize() {
    // TODO: implement method

    return 0;
  }

  @Override
  public String getPath() {
    // TODO: implement method
    return null;
  }

  @Override
  public void setParent(Directory parent) {
    // TODO: implement method

  }
  public void add(Component c) {
    // TODO: implement method

  }
  public void remove(Component c) {
    // TODO: implement method
  }

  @Override
  public String toString() {
    // TODO: implement method
  return super.toString();
  }
}
